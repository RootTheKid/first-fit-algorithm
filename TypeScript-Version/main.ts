import { Host } from './lib/Host';
import { VM } from './lib/VM';
import { BinPacker } from './lib/BinPacker';
import Debug from 'debug';
const debug = Debug("FirstFit");

const hosts: Host[] = [
  new Host('HM1', 600, 8, 20),
  new Host('HM2', 100, 8, 10),
  new Host('HM3', 200, 4, 20)
];
const vms: VM[] = [
  new VM('Database1', 500, 4, 4),
  new VM('Redis1', 100, 1, 20),
  new VM('Redis2', 0, 2, 8),
  new VM('Web1', 10, 4, 1),
  new VM('Web2', 50, 4, 3),
  new VM('Web3', 5, 1, 1),
  new VM('Database2', 150, 6, 10),
  new VM('Database3', 150, 2, 5)
];

BinPacker.firstFit(hosts, vms, new Host('newHost', 500, 8, 15));
debug(hosts);
debug(vms);
