import { HostingEnvirnoment } from "./HostingEnvirnoment";
import { VM } from "./VM";

export class Host extends HostingEnvirnoment {
  AssignedVMs: VM[] = [];
  get FreeCPUCores(): number { return this.CPUCores - this.AssignedVMs.map(vms => vms.CPUCores).reduce((a, b) => a + b, 0); }
  get FreeRAM(): number { return this.RAM - this.AssignedVMs.map(vms => vms.RAM).reduce((a, b) => a + b, 0); }
  get FreeHDD(): number { return this.HDD - this.AssignedVMs.map(vms => vms.HDD).reduce((a, b) => a + b, 0); }
}