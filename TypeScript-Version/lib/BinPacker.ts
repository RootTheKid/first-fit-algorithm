import { Host } from "./Host";
import { VM } from "./VM";
import Debug from 'debug';
const debug = Debug("FirstFit:BinPacker");

export namespace BinPacker {
  export function firstFit(hosts: Host[], vms: VM[], templateHost?: Host): void {
    let newHostIdx: number = 0;
    fit: for (let vm of vms) {
      for (let host of hosts) {
        if (couldVmFit(vm, host)) {
          addVm2Host(vm, host);
          continue fit;
        }
      }

      if (typeof templateHost === 'undefined')
        throw new Error(`Couldn't map VM: ${vm.Name}`);

      const newHost: Host = new Host(templateHost.Name + ++newHostIdx, templateHost.HDD, templateHost.CPUCores, templateHost.RAM);

      if (!couldVmFit(vm, newHost))
        throw new Error(`Couldn't map VM: ${vm.Name}`);

      addVm2Host(vm, newHost);
      hosts.push(newHost);
    }
  }

  function couldVmFit(vm: VM, host: Host): boolean {
    return (host.FreeHDD >= vm.HDD &&
      host.FreeCPUCores >= vm.CPUCores &&
      host.FreeRAM >= vm.RAM) ? true : false;
  }

  function addVm2Host(vm: VM, host: Host): void {
    host.AssignedVMs.push(vm);
    vm.Mapped = true;
  }
}

