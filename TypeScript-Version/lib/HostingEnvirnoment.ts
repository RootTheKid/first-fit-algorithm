export class HostingEnvirnoment {
  Name: string;
  CPUCores: number;
  RAM: number;
  HDD: number;
  constructor(Name: string, HDD: number, CPUCores: number, RAM: number) {
    this.Name = Name;
    this.CPUCores = CPUCores;
    this.RAM = RAM;
    this.HDD = HDD;
  }
}
