import * as tap from 'tap';
import { BinPacker } from '../lib/BinPacker';
import { Host } from '../lib/Host';
import { VM } from '../lib/VM';

tap.test('FirstFit VMs just fit Hosts', (t) => {
  const hosts: Host[] = [new Host('HM1', 600, 8, 25)];
  const vms: VM[] = [
    new VM('VM1', 500, 4, 4),
    new VM('VM2', 100, 1, 20),
  ];
  t.error(BinPacker.firstFit(hosts, vms));
  t.end();
});

tap.test('FirstFit add new Host to fit VMs', (t) => {
  const hosts: Host[] = [new Host('HM1', 600, 8, 25)];
  const vms: VM[] = [
    new VM('VM1', 500, 4, 4),
    new VM('VM2', 100, 1, 20),
    new VM('VM3', 150, 6, 10)
  ];
  tap.error(BinPacker.firstFit(hosts, vms, new Host('newHost', 500, 8, 15)));
  t.end();
});

tap.test('FirstFit Error coudnt map VM and no template host', (t) => {
  const hosts: Host[] = [new Host('HM1', 600, 8, 20)];
  const vms: VM[] = [new VM('VM1', 650, 4, 4)];
  tap.throws(() => { BinPacker.firstFit(hosts, vms); });
  t.end();
})

tap.test('FirstFit Error coudnt map VM with template host', (t) => {
  const hosts: Host[] = [new Host('HM1', 600, 8, 20)];
  const vms: VM[] = [
    new VM('Database2', 700, 6, 10)
  ];
  tap.throws(() => { BinPacker.firstFit(hosts, vms, new Host('newHost', 500, 8, 15)); });
  t.end();
})
