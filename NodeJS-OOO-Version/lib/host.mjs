class Host {
    constructor (name, hdd, cpuCores, ram) {
        this.name = name;
        this.hdd = hdd;
        this.ram = ram;
        this.cpuCores = cpuCores;
        this.assignedVms = [];
    }

    addVm(vm) {
        if (this.hdd >= vm.hdd && this.cpuCores >= vm.cpuCores && this.ram >= vm.ram) {
            this.assignedVms.push(vm);
            this.hdd -= vm.hdd;
            this.cpuCores -= vm.cpuCores;
            this.ram -= vm.ram;
            return true;
        }
        return false;
    }
}

export {Host}