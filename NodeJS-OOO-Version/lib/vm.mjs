class VM {
    constructor(name, hdd, ram, cpuCores) {
        this.name = name;
        this.hdd = hdd;
        this.ram = ram;
        this.cpuCores = cpuCores;
        this.mapped = false;
    }
}

export { VM }