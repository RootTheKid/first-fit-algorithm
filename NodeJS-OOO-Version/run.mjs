import { hosts } from './assets/hosts.mjs'
import { vms } from './assets/input-vms.mjs'
import { firstFit } from './firstFit.mjs'
import { Host } from './lib/host.mjs'
// import { VM } from './lib/vm.mjs'

firstFit(hosts, vms, new Host('newHost', 140, 8, 15))

// firstFit([], [new VM(...)], new Host('tamplteThatnot fin in VM'))

console.log(hosts)