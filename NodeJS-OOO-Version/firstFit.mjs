import { Host } from "./lib/host.mjs";

/**
 * 
 * @param {Host[]} hosts 
 * @param {VM[]} vms 
 */
function firstFit(hosts, vms, templateHost) {
    let newHostIdx = 0;
    myLoop: for (let index = 0; index < vms.length; index++) {
        const vm = vms[index];
        for (let indexHost = 0; indexHost < hosts.length; indexHost++) {
            const host = hosts[indexHost];
            if (host.addVm(vm))
                continue myLoop;
        }

        if(typeof templateHost === 'undefined')
            throw `Couldn't map VM: ${vm.name}`;

        let newHost = Object.assign(new Host, templateHost)

        if (!newHost.addVm(vm))
            throw `Couldn't map VM: ${vm.name}`;
        newHost.name += ++newHostIdx;
        hosts.push(newHost)
    }

}

export { firstFit }