import { Host } from '../lib/host.mjs'
const hosts = [
  new Host('HM1', 600, 8, 20),
  new Host('HM2', 100, 8, 10),
  new Host('HM3', 200, 4, 20)
]

export { hosts }