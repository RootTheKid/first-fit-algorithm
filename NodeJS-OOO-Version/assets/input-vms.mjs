import { VM } from '../lib/vm.mjs'
const vms = [
  new VM('Database1', 500, 4, 4),
  new VM('Database2', 150, 10, 6),
  new VM('Database3', 150, 10, 6),
  new VM('Redis1', 100, 20, 1),
  new VM('Redis2', 0, 8, 2),
  new VM('Web1', 10, 1, 4),
  new VM('Web2', 50, 3, 4),
  new VM('Web3', 5, 1, 1)
]

export { vms }