const hosts = require('./assets/hosts.json')
const inputVms = require('./assets/input-vms.json')

inputVms.forEach(vm => {
  vm.mapped = false
  hosts.forEach(host => {
    if (!vm.mapped) {
      if (host.hdd >= vm.hdd && host.cpuCores >= vm.cpuCores && host.ram >= vm.ram) {
        host.hdd -= vm.hdd
        host.cpuCores -= vm.cpuCores
        host.ram -= vm.ram
        vm.mapped = true
        if(host.hasOwnProperty('assignedVms'))
          host.assignedVms.push(vm.name)
        else {
          host.assignedVms = []
          host.assignedVms.push(vm.name)
        }
      }
    }
  })
})

vmsNotMapped = []
inputVms.forEach(vm => {
  if(!vm.mapped)
    vmsNotMapped.push(vm)
});

console.log('Host VM mapping:')
console.log(hosts)
console.log('VMs that couldn\'t be mapped')
console.log(vmsNotMapped)
