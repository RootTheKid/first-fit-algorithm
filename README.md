# First-Fit-Algorithm

Little coding challenge to implement a first fit algorithm.  
This is just a first approach of the algorithm.  
E.g. in a later version a class definition for the Hosts and VMs could be used.

I chosed JSON as input format because a REST-Service would provide a simular input.

## TypeScript Version

TypeScript Version of FirstFit Algorithm.

> Test Status:  
[![pipeline status](https://gitlab.com/RootTheKid/first-fit-algorithm/badges/master/pipeline.svg)](https://gitlab.com/RootTheKid/first-fit-algorithm/-/commits/master)
[![coverage report](https://gitlab.com/RootTheKid/first-fit-algorithm/badges/master/coverage.svg)](https://gitlab.com/RootTheKid/first-fit-algorithm/-/commits/master)

### Get Started

## Get Started

To easaly run this project just execute

```bash
docker-compose up run
```

## Dev with Docker

To start the development docker container use

```bash
docker-compose up dev
```

A container then starts which keeps a zsh open.  
A shell in this container can be spawned via

```bash
docker-compose exec dev zsh
```

Afterwards commands like node, etc can be executed.

Or just use the VSCode Remote Contianer Extension.
