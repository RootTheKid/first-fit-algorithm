FROM node:lts-alpine3.9 as base

# Development Docker Container
FROM base AS dev
WORKDIR /usr/src/app
RUN apk add --update zsh git vim
RUN sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
COPY zshrc /root/.zshrc

# TypeScript Dev
FROM dev AS dev-ts
WORKDIR /usr/src/app
RUN npm install -g ts-node

# Execute TS in Docker
FROM base AS run-ts
WORKDIR /usr/src/app
RUN npm install -g ts-node
COPY ./TypeScript-Version .
ENV DEBUG *
ENV DEBUG_DEPTH 10
CMD [ "ts-node", "src/main.ts" ]

# Execution Docker Container
FROM base AS run-node
WORKDIR /usr/src/app
COPY ./NodeJS-Version .
CMD [ "node", "index.js" ]